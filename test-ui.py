import unittest
from os import environ
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

WEBSITE = "http://you.regettingold.com"
is_local = bool(environ.get("IS_LOCAL", ""))

class TestPythonWebsite(unittest.TestCase):

    def setUp(self):
        if is_local:
            self.driver = webdriver.Chrome()
        else:
            ops = Options()
            ops.add_argument("--no-sandbox")
            ops.add_argument("--disable-dev-shm-usage")
            self.driver = webdriver.Remote(
                command_executor="http://selenium__standalone-chrome:4444/wd/hub",
                options=ops,
            )

    def test_search(self):
        driver = self.driver
        driver.get(WEBSITE)
        dd = driver.find_element_by_name("dd")
        mm = driver.find_element_by_name("mm")
        yy = driver.find_element_by_name("yy")

        dd.send_keys(25)
        mm.send_keys(12)
        yy.send_keys(2000)

        button = driver.find_element_by_name("submitButton1")

        button.click()

        
        self.assertIn("28th Sep 2055", driver.page_source)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()